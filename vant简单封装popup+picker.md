main.js
```javascript
import {
    Popup,
    Picker,
} from 'vant'
import vSelect from "./components/select.vue";

Vue.use(Popup)
Vue.use(Picker)

Vue.component("v-select", vSelect);
```

select.vue
```vue
<template>
  <van-popup class="_popup" v-model="visible.show" :closeOnClickModal="false" position="bottom">
    <div class="popup-container">
      <div class="title">
        <div class="text" @click="cancel()">取消</div>
        <div class="text confirm" @click="confirm()">确定</div>
      </div>
      <van-picker ref="picker" class="_picker" value-key="name" :columns="slots" @change="onChange"></van-picker>
      <div class="my-border"/>
    </div>
  </van-popup>
</template>

<script>
export default {
  components: {},
  props: {
    slots: {
      type: Array,
      required: true
    },
    visible: {
      // type: Object,
      required: true
    }
  },
  methods: {
    //滚轮变化回调
    onChange(picker, values) {
      // console.log(values)
    },

    //取消按钮
    cancel() {
      this.visible.show = false;
    },

    //确定按钮
    confirm() {
      this.visible.show = false;
      let values = this.$refs.picker.getValues();
      //给父组件传值
      this.$emit("values", values);
    }
  }
};
</script>

<style scoped lang="less">
._popup {
  width: 100%;
  .popup-container {
    width: 100%;
    height: 100%;
    background-color: white;
    .title {
      display: flex;
      justify-content: space-between;
      .text {
        font-size: 18px;
        padding: 10px 20px;
        color: #777;
      }
      .confirm {
        color: #569eff;
      }
    }
    .my-border {
      width: 100%;
      height: 44px;
      background: rgba(0,0,0,0.1);
      position: absolute;
      top: 50%;
    }
  }
}
</style>
```

demo.vue
```vue
<template>
<v-select :slots="selectDatas" :visible="iSshowSelect" @values="onReceiveData"></v-select>
<div @click="showSelect">点击选择</div>
</template>
export default {
    data() {
        iSshowSelect: { show: false },
        selectData: {id: -1, name: '全部'},
        selectDatas: [
            {
                flex: 1,
                values: [
                    { id: -1, name: '全部' },
                    { id: 1, name: '北京西城食品经销商' },
                    { id: 2, name: '杭州双子卡啦卡啦食品商贸经销有限公司' },
                    { id: 3, name: '成都可拉卡啦经销商' },
                    { id: 4, name: '上海礼贸经销商' },
                ],
            },
        ],
    },

    methods: {    
        //点击确定之后接收选择的值
        onReceiveData(values) {
            this.selectData = values[0]
            console.log('selectData: ', JSON.stringify(this.selectData))
        },

        showSelect() {
            this.iSshowSelect = { show: true }
        },
    },
}
```

```javascript

```

```javascript

```